import { customRef } from 'vue'

export function useDebouncedRef(value:any, delay = 30000) {
  let timeout:any;
  return customRef((track, trigger) => {
    return {
      get() {
        track()
        return value
      },
      set(newValue) {
        clearTimeout(timeout)
        timeout = setTimeout(() => {
          value = newValue
          trigger()
        }, delay)
      }
    }
  })
}

type CallbackFn = (item?: any) => void
let timer: any = null
export default function (Callback: CallbackFn, delay = 1000) {
  timer != null ? clearTimeout(timer) : null
  timer = setTimeout(() => {
    Callback && Callback() //当有值才会执行
  }, delay)
}

